
## Cloner le repository

Vous pouvez vous aider de ce [lien](https://confluence.atlassian.com/x/4whODQ).

1. Cliquer qur le bouton *clone* qui se trouve au dessus de la description du repository.
2. choisissez **sourcetree** ou **Vs Code** afin de cloner le repository. 

## Installations 

Toutes nos installations ont été effectuées sous Linux.

Voici le [lien](https://docs.zephyrproject.org/latest/getting_started/index.html) pour installer l'os zephyr et ces dépendances.

**Informations utiles**

1. Nom du périphérique: **SEGGER J-LINK**
2. Nom de la carte: **nrf52840dk_nrf52840**
3. Port: **/dev/ttyACM0**
4. Bps: **115200**

Une fois l'os zephyr installé, ouvrez 2 terminals.
sur le terminal 1, nous allons effectuer le build et le flashage de la carte nordic.
sur le terminal 2, nous allons afficher les données entrantes  et sortantes de la carte.

***Build et flash de la carte nordic***

Sur le terminal 1, tapez les commandes suivantes:

***

- cd ~/zephyrproject/zephyr  
- west build -p auto -b 'your-board-name' samples/basic/blinky; ce qui donnerait dans notre cas (west build -p auto -b nrf52840dk_nrf52840 samples/basic/blinky)   
**Mettre le dossier (dézipper) dans /samples** 
- west build -b nrf52840dk_nrf52840 samples/driver-uwb
- west flash  

if nrfjprog is asked for flashing do

- ln -s ~/nordic/nRF-Command-Line-Tools_10_12_0/nrfjprog/nrfjprog  /usr/local/bin/nrfjprog



***Affichage des données entrantes et sortantes***

Sur le terminal 2, taper la commande:
1. ***minicom -o -s -w***
1. puis dans config du port série taper A et et remplacer /dev/modem par /dev/ttyACM0 entrer pour valider 
1. et taper F pour mettre flux matériel à non et entrer pour valider 
1. flasher la board sur le terminal 1 et faire echap pour lancer minicom


---
 **minicom -D /dev/ttyACM0 -b 115200**   

Dans votre fenetre *minicom* votre devriez avoir des données qui rentrent/sortent; par exemples, les printk() du main.  
Biensur il faudrait que vous ayez installé *minicom* et que vous puissiez compiler avec west 

**Pins Mapping **

- NXP(UWB)        -->  Nordic(NRF)
- Sur P5:
- Nom_Pin Numéro  -->   Nom_Pin    Numéro
- RDY_N_1   5     -->   PIN_RDY      23     //voir NCJ29D5_config.c
- INT_N_1   8     -->   PIN_INT      24     //voir NCJ29D5_config.c
- CS_1     14     -->   GPIO_CS_PIN  14     //voir NCJ29D5_config.c
- SDI_1    11     -->   mosi-pin     20     //voir ~/zephyrproject/zephyr/boards/arm/nrf52840dk_nrf52840/nrf52840dk_nrf52840.dts
- SCLK_1    9     -->   sck-pin      19 
- SDO_1     7     -->   miso-pin     21 
- RST       6     -->   RESET 
- 3.3V            -->   VDD
- GND             -->   GND

**Fichiers importants à éditer**

---
nrf52840dk_nrf52840_defconfig  présent dans zephyrproject/zephyr/boards/arm/nrf52840dk_nrf52840

---
prj.conf                       présent dans zephyrproject/zephyr/samples/driver-uwb

---
!!! le dossier **driver-uwb** étant le dossier créé pour ce projet.

---
CMakeLists.txt                 présent dans zephyrproject/zephyr/samples/driver-uwb

---
et les autres fichiers présents dans le dossier **driver-uwb** 
le main les fichiers .c et les headers files mais les premiers fichiers énoncés ci-dessus forment la base pour bien 
configurer l'environnement avant de coder dessus.
Si l'environnement doit être modifié ils doivent l'être par conséquent.
Sinon des erreurs peuvent survenir ou des warnings qui aident à résoudre le problème.
NB: tous les warnings relatifs au fichiers .c et .h ne sont pas forcément bloquant mais doivent être étudiés.



